var container, stats ;  
var camera, scene, renderer, controls;
var mouse = new THREE.Vector2(), INTERSECTED;
var alllines = new THREE.Object3D();

init();
animate();

function init() {

    container = document.createElement( 'div' );
    document.body.appendChild( container );

    // renderer
    renderer = new THREE.WebGLRenderer();
    renderer.setClearColor( 0xDADADA );
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize( window.innerWidth, window.innerHeight );

    renderer.sortObjects = false;
    container.appendChild(renderer.domElement);

    var info = document.createElement( 'div' );
    info.style.position = 'absolute';
    info.style.top = '10px';
    info.style.width = '100%';
    info.style.textAlign = 'center';

    // camera
    camera = new THREE.PerspectiveCamera( 70, window.innerWidth / window.innerHeight, 1, 10000 );
    camera.position.set(-4,0.4,13);
    camera.up.set( 0.0, 1.0, 0.0);

    // controls
    controls = new THREE.OrbitControls( camera, renderer.domElement );
    controls.target.set( 0.0, 0.0, 8);

    // scene
    scene = new THREE.Scene();

    // Axes
    //axes = buildAxes();
    //scene.add( axes );

    // light
    scene.add( new THREE.AmbientLight( 0xffffff, 0.2 ) );
    var light = new THREE.DirectionalLight( 0xffffff, 2 );
    light.position.set( 30, 10, 1 ).normalize();
    scene.add( light );

    // Detector
    var detectormaterial = new THREE.MeshLambertMaterial({color: 0x40d0ff, blending: THREE.NormalBlending, transparent: true, opacity: 0.1, depthTest: true, side: THREE.DoubleSide });

    // cube 1
    var cubeGeometry1 = new THREE.BoxGeometry(3,1,1); 
    var cube1 = new THREE.Mesh(cubeGeometry1, detectormaterial);
    scene.add( cube1 );

    //cube 2
    var cubeGeometry2 = new THREE.BoxGeometry(0.5,5,5);
    var cube2 = new THREE.Mesh(cubeGeometry2, detectormaterial);
    cube2.position.set(10, 0, 0); //X,Y,Z //R,V,B
    scene.add( cube2 );

    //cube 3
    var cubeGeometry3 = new THREE.BoxGeometry(0.5, 8, 8);
    var cube3 = new THREE.Mesh(cubeGeometry3, detectormaterial);
    cube3.position.set(12, 0, 0);
    scene.add( cube3 );

    //cube 4
    var cubeGeometry4 = new THREE.BoxGeometry(0.5, 10, 10);
    var cube4 = new THREE.Mesh(cubeGeometry4, detectormaterial);
    cube4.position.set(14, 0, 0);
    scene.add(cube4);

    // Particles
    // line 1
    var linegeometry1 = new THREE.Geometry();
    var line1 = new THREE.Line( linegeometry1, new THREE.LineBasicMaterial({color: 0x00033}) );
    linegeometry1.vertices.push(new THREE.Vector3( -7, -1, 0) );
    linegeometry1.vertices.push(new THREE.Vector3( 700, 100, 0) );
    alllines.add(line1);

    // line 2
    var linegeometry2 = new THREE.Geometry();
    var line2 = new THREE.Line( linegeometry2, new THREE.LineBasicMaterial({color: 0x00033}) );
    linegeometry2.vertices.push(new THREE.Vector3( -5, -1, 0) );
    linegeometry2.vertices.push(new THREE.Vector3( 500, 100, 0) );
    alllines.add(line2);

    //line 3
    var linegeometry3 = new THREE.Geometry();
    var line3 = new THREE.Line( linegeometry3, new THREE.LineBasicMaterial({color: 0x00033}) );
    linegeometry3.vertices.push(new THREE.Vector3( -1, 0.25, 0) );
    linegeometry3.vertices.push(new THREE.Vector3( 100, -25, 0) );
    alllines.add(line3);

    scene.add(alllines);

    raycaster = new THREE.Raycaster();

    document.addEventListener( 'mousedown', onDocumentMouseDown, false );   
    window.addEventListener( 'resize', onWindowResize, false );
}

function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize( window.innerWidth, window.innerHeight );
}

function onDocumentMouseDown( event ) {
    event.preventDefault();
    mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
    mouse.y = - ( event.clientY / window.innerHeight ) * 2 + 1;

    // find intersections
    raycaster.setFromCamera( mouse, camera );
    raycaster.linePrecision = 0.1;
    var intersects = raycaster.intersectObjects( alllines.children );

    if ( intersects.length > 0 ) {
        console.log("test1"+intersects[0]);
        if ( INTERSECTED != intersects[ 0 ].object ) {
            console.log('objects id'+intersects[ 0 ].object.id);
            console.log('objects name'+intersects[ 0 ].object.name);

            if ( INTERSECTED ) INTERSECTED.material.color.setHex( INTERSECTED.currentHex );

            INTERSECTED = intersects[ 0 ].object;
            INTERSECTED.currentHex = INTERSECTED.material.color.getHex();
            //INTERSECTED.material.emissive.setRGB(.5, .5, 0);
            INTERSECTED.material.color.setHex(0xF1FF6E);
            console.log(intersects.length);
        }
    } else {
      if ( INTERSECTED ) INTERSECTED.material.color.setHex( INTERSECTED.currentHex );
      INTERSECTED = null;
    }
}

function animate() {
    requestAnimationFrame( animate );
    render();
} 

function render() 
{
    renderer.render( scene, camera );
}

function buildAxes() {
    var axes = new THREE.Object3D();

    axes.add( buildAxis( new THREE.Vector3( 0, 0, 0 ), new THREE.Vector3( 100, 0, 0 ), 0xFF0000, false ) ); // +X
    axes.add( buildAxis( new THREE.Vector3( 0, 0, 0 ), new THREE.Vector3( -100, 0, 0 ), 0x800000, true) ); // -X
    axes.add( buildAxis( new THREE.Vector3( 0, 0, 0 ), new THREE.Vector3( 0, 100, 0 ), 0x00FF00, false ) ); // +Y
    axes.add( buildAxis( new THREE.Vector3( 0, 0, 0 ), new THREE.Vector3( 0, -100, 0 ), 0x008000, true ) ); // -Y
    axes.add( buildAxis( new THREE.Vector3( 0, 0, 0 ), new THREE.Vector3( 0, 0, 100 ), 0x0000FF, false ) ); // +Z
    axes.add( buildAxis( new THREE.Vector3( 0, 0, 0 ), new THREE.Vector3( 0, 0, -100 ), 0x000080, true ) ); // -Z

    return axes;

    }

function buildAxis( src, dst, colorHex, dashed ) {
    var geom = new THREE.Geometry(), mat; 

    if(dashed) {
        mat = new THREE.LineDashedMaterial({ linewidth: 1, color: colorHex, dashSize: 5, gapSize: 5 });
    } else {
        mat = new THREE.LineBasicMaterial({ linewidth: 1, color: colorHex });
    }

    geom.vertices.push( src.clone() );
    geom.vertices.push( dst.clone() );

    var axis = new THREE.Line( geom, mat );

    return axis;
}